Instagram Challenge
===================

On their `blog`_ Instagram's engineering team posted a small `Python
challenge`_ to reconstruct a shredded image into it's original form.

I'm coming late to the party so no more t-shirts, but it is still an
interesting problem to tackle.

The test image they gave is:

.. image:: https://bitbucket.org/thesociable/instagram/raw/18e64688e846/resource/shredded.png

They stress simplicity over fancy algorithms and have the concession that 
the shred width does not have to be calculated.

So, with that in mind, you can run my basic solution as follows::

    $ python source/unshred.py resource/shredded.png resource/unshredded.png

That should produce the following unshredded image:

.. image:: https://bitbucket.org/thesociable/instagram/raw/9489f2ed7afb/resource/unshredded.png

The approach I took was to loop over all the shreds and for each one
find the best left neighbour shred. This was done by comparing the euclidean
distance between the left pixel column of the shred and the right pixel column
of the candidate shred.

With this neighbour registry in place I then had to work out which was the 
first shred in the image as they were randomly jumbled. To do this I walk 
over the neighbour registry trying each shred as a potential start shred. The
longest walk through the registry (before encountering a duplicate use of a 
shred) is then the best solution for ordering the shreds to get the unshredded
image.


.. _blog: http://instagram-engineering.tumblr.com/
.. _Python challenge: http://instagram-engineering.tumblr.com/post/12651721845/instagram-engineering-challenge-the-unshredder


