# :coding: utf-8

import argparse
import math
import operator
from PIL import Image


def _get_distance(a, b):
    '''Return euclidean distance between points *a* and *b*'''
    return math.sqrt(
        sum([math.pow(component, 2) 
             for component in map(operator.sub, a, b)])
    )


def process(image, shred_width):
    '''Process shredded *image* returning a new unshredded image.
    
    *shred_width* should indicate the width of each shred.
    
    '''
    width, height = image.size
    data = image.getdata()
    shred_count = width/shred_width
    
    # Compare each shred's left edge to every other shred's right edge. 
    # Store the candidate shred that has the smallest difference. Difference
    # is calculated as the average euclidean distance between the rgba values
    # for each left and right pixel pair.
    neighbours = {}
    
    for shred in range(shred_count):
        neighbours[shred] = neighbour = {}

        left = [data[(shred*shred_width)+(y*width)] for y in range(height)]
        
        for candidate in range(shred_count):
            if candidate == shred:
                continue

            else:
                right = [data[((candidate+1)*shred_width)-1+(y*width)] 
                         for y in range(height)]
                
                distance = sum(map(_get_distance, left, right)) / height
                if not neighbour or neighbour['distance'] > distance:
                    neighbour['distance'] = distance
                    neighbour['shred'] = candidate

    # As we don't yet know which shred is the start shred, walk through 
    # neighbours for each possible start shred. The longest walk should be the
    # solution.
    solution = []
    for index in range(shred_count):
        path = [index]
        neighbour = neighbours[path[0]]['shred']
        while neighbour not in path:
            path.append(neighbour)
            neighbour = neighbours[neighbour]['shred']
        
        # Best solution?
        if len(path) > len(solution):
            solution = path
    
    # Left edge was compared so reverse solution to get correct order
    solution.reverse()

    # Generate new image with shreds arranged according to solution
    unshredded = Image.new('RGBA', (width, height))
    for target, shred in enumerate(solution):
        shred = image.crop((shred*shred_width, 0, 
                            (shred+1)*shred_width, height))

        unshredded.paste(shred, (target*shred_width, 0))

    return unshredded


def main(arguments=None):
    '''Main entry point for unshred'''
    parser = argparse.ArgumentParser(description='Unshred image.')
    parser.add_argument('input', help='Path to shredded source image.')
    parser.add_argument('output', help='Path to write unshredded output to.')
    parser.add_argument('-w', '--shred-width', type=int, default=32,
                        help='Pixel width of each shred in shredded image.')
    
    namespace = parser.parse_args(arguments)

    shredded = Image.open(namespace.input)
    unshredded = process(shredded, shred_width=namespace.shred_width)
    unshredded.save(namespace.output)


if __name__ == '__main__':
    raise SystemExit(main())

